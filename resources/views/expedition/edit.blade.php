@extends('layouts.app')

@section('content')
<div class="container">
<div class="container" style="height: 25px"></div>
  <form action="{{route('updateExpedition', ['expedition' => $expedition->id])}}" files="true" class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="x_title">
      <h2>Editar Expedici&oacute;n</h2>
    </div>
    <div class="container" style="height: 25px"></div>
    <div class="x_content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12"><h3>T&iacute;tulo<span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" name="titulo" id="titulo" required="required" class="form-control col-md-7 col-xs-12" value="{{$expedition->titulo}}">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
          <div class="form-group">
            <div class="profile_img">
                <!-- end of image cropping -->
                <label for="logo"><h3>Logo de Expedici&oacute;n</h3></label>
                @if($expedition->ruta_img)
                <div class="container-img"><img id="cambiar_imagen" src="{{$expedition->ruta_img}}" class="imagen" /> </div>
                @else
                <div class="container-img"><img id="cambiar_imagen" src="/./images/unknown.jpg" class="imagen" /> </div>
                @endif
                <input class="btn " id="imagen" name="imagen" type="file" onchange="readURL(this);">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
          <div class="form-group">
            <div class="profile_img">
                <!-- end of image cropping -->
                <label for="logo"><h3>Mapa de Expedici&oacute;n</h3></label>
                @if($expedition->ruta_map)
                <div class="container-img"><img id="cambiar_imagen1" src="{{$expedition->ruta_map}}" class="imagen" /> </div>
                @else
                <div class="container-img"><img id="cambiar_imagen1" src="/./images/unknown.jpg" class="imagen" /> </div>
                @endif
                <input class="btn " id="imagen1" name="imagen1" type="file" onchange="readURL(this);">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12"><h3>Descripci&oacute;n <span class="required">*</span></label></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <textarea id="descripcion" class="form-control col-md-7 col-xs-12" name="descripcion" rows="8" cols="80">{{$expedition->descripcion}}</textarea>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group pull-right">
                  <button type="submit" class="btn btn-success">Guardar</button>
                  <a href="{{route('showExpedition', ['expedition' => $expedition->id])}}" class="btn btn-primary">Cancelar</a>
              </div>
          </div>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
        function readURL(input) {
          console.log(input.id)
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                  if(input.id == 'imagen')
                    $('#cambiar_imagen').attr('src', e.target.result);
                  else if (input.id == 'imagen1')
                    $('#cambiar_imagen1').attr('src', e.target.result);

                }
                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection
