@extends('layouts.app')

@section('content')
<div class="container">
  @if(Auth::check())
  <div class="row">
    <div class="col-sm-1">
      <a title="editar" href="{{ route('editExpedition', ['expedition' => $expedition->id] ) }}" class="btn btn-primary"><i class="far fa-edit" aria-hidden="true"></i></a>
    </div>
    <div class="col-sm-1">
      <a title="eliminar" class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$expedition->id}}"><i class="fa fa-trash"></i></a>
    </div>
  </div>
  @endif
  <div class="row">
    <div class="col-xs-4 col-sm-4">
      <h1>{{$expedition->titulo}}</h1>
    </div>
  </div>
  <div class="container" style="height: 25px"></div>
  <div class="row">
  @if($expedition->ruta_img)
    <div class="col-xs-4 col-sm-4">
      <div class="container-img"><img src="{{$expedition->ruta_img}}"> </div>
    </div>
  @endif
  </div>
  <div class="container" style="height: 25px"></div>
  <div class="row">
    <div class="col-xs-4 col-sm-8">
      <p>{{$expedition->descripcion}}</p>
    </div>
  </div>
  <div class="container" style="height: 25px"></div>
  <div class="row">
    @if($expedition->ruta_map)
      <div class="col-xs-4 col-sm-4">
        <div class="container-img"><img src="{{$expedition->ruta_map}}"> </div>
      </div>
    @endif
  </div>
  <div class="container" style="height: 50px"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-group pull-right">
        <a class="btn btn-primary" href="{{ route('listExpedition') }}">Volver</a>
      </div>
    </div>
  </div>

</div>
@endsection
