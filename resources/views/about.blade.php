@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-4 col-sm-4">
        <img src="images/trisquel.png">
      </div>
      <div class="col-xs-8 col-sm-8">
        <h1 style="color: blue"><b>Expedici&oacute;n Libertad </b> </h1>
        <p>Zoon politikon de Aristoteles. El hombre es un animal social. </p>
        <p>Vivimos obligados a aparentar lo que muchas veces no sentimos con el &uacute;nico fin de no ser expulsados de nuestro entorno social. Vivimos solo para agradar a los dem&aacute;s. Para compararnos con los dem&aacute;s. Corremos una carrera que nunca se termina en b&uacute;squeda de cumplir objetivos que no tienen mucho sentido. Podemos vivir toda una vida sin preguntarnos jam&aacute;s que es lo que realmente queremos.
        Usted y yo somos mortales. El momento es ahora. Basta de excusas. Este es el camino que a m&iacute; me hace feliz. Elija su camino y rec&oacute;rralo por m&aacute;s que no sea el que todos recorren. No espere que los dem&aacute;s le digan c&oacute;mo debe ser, hacer o sentir. Usted ya lo sabe. Lib&eacute;rese.
        Este es mi camino, bienvenido. Dejo toda la aparente seguridad para vivir lo que s&eacute; que tengo que vivir. Navegue, descubra, r&iacute;a y llore conmigo. Necesito su compañ&iacute;a.
        </p>
      </div>
    </div>
    <div class="container" style="height: 50px"></div>
    <div class="row">
      <div class="col-xs-4 col-sm-4">
        <img src="images/Portis.jpg" height="320px" width="360px">
      </div>
      <div class="col-xs-4 col-sm-8">
        <h1 style="color: blue"><b>Bruno Edgar Portis Ricciardi</b> </h1>
        <p>Vivo en General Pico La Pampa, soy abogado y docente de economía, historia, derecho, sociología, entre otras.</p>
        <p>Tengo 35 años y entendí que ahora puedo hacer esto gracias a hablar con personas que me mostraron que no hay nada peor que dejar que los sueños queden sin cumplirse.
        </p>
        <p>No tengo más que decir. Que los hechos hablen. Res non verba.</p>
      </div>
    </div>
  </div>
@endsection
