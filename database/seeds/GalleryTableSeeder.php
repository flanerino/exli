<?php

use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('galeria')->insert([
         'titulo' => str_random(10),
         'expedicion_id' => 1,
         'ruta_img' => str_random(10),
         'descripcion' => str_random(10),
     ]);
    }
}
